﻿weatherForecast.controller('weatherController', ['$scope', 'httpGET', function ($scope, httpGET) {

    $scope.loadDataByCity = function (cityname) {
        var getWeatherForecastUrl = "/Home/GetWeatherForecast";
        var getCurrentWeatherUrl = "/Home/GetCurrentWeather";
        httpGET(cityname, getCurrentWeatherUrl).success(function (data) {
            if (data === "404") {
                window.location.href = "/Home/Error"
            } else {
                $('#loader-div').show();
                var deserializeData = angular.fromJson(data);
                setDataInScope(deserializeData);

                httpGET(cityname, getWeatherForecastUrl).success(function (data) {
                    var forecast = angular.fromJson(data);
                    $scope.data = forecast.list;
                    $('#loader-div').hide();
                });
            }
        })
    }
    function setDataInScope(data) {
        $scope.city = data.name;
        $scope.country = data.sys.country;
        $scope.img = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";
        $scope.temperature = data.main.temp;
        $scope.weatherDescription = data.weather[0].description;
        $scope.clouds = data.clouds.all;
        $scope.pressure = data.main.pressure;
        $scope.wind = data.wind.speed;
        $scope.visibility = data.visibility;
    }
}]);