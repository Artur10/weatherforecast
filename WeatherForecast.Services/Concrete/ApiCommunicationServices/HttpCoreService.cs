﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;

namespace WeatherForecast.Services.ApiCommunicationServices
{
    public class HttpCoreService:IHttpCoreService
    {
        public string CreateUrl(string city,int days,string path)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = "http";
            uriBuilder.Host = ConstantsConfig.Host;
            uriBuilder.Path = path;
            uriBuilder.Query = string.Format(ConstantsConfig.Query, city, days);

            return uriBuilder.ToString();
        }

        public async Task<string> GetAsynk(string url)
        {
            string content;
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);

            if (response.IsSuccessStatusCode)
            {
                content = await response.Content.ReadAsStringAsync();
                return content;
            }
            else
            {
                content = "404";
            }
            return content;
        }
    }
}
