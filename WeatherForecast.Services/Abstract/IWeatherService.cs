﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Services
{
    public interface IWeatherService
    {
         Task<string> GetWeatherForecast(string city,int days = 7);

         Task<string> GetCurrentWeather(string city, int days = 7);
    }
}
