﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Services
{
    public static class ConstantsConfig
    {
        public static string Host = "api.openweathermap.org";
        public static string ForecastPath = "/data/2.5/forecast/daily";
        public static string CurrentWeatherPath = "/data/2.5/weather";
        public static string Query = "q={0}&mode=json&units=metric&cnt={1}&APPID=a0852be5bccd7a05a90f8373202d8f1f";
    }
}
